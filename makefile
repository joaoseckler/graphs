CPPFLAGS=-Wall -g -Wpedantic -O0
INCLUDE=graph.o graph.h bag.h st.h rn.h

ep1: interface.cpp $(INCLUDE)
	g++ $(CPPFLAGS) $< $(INCLUDE) -o $@

.PHONY: clean
clean:
	rm -rf *.o ep1 tests/
