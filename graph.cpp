#include "graph.h"

/* *************** Graph ********************* */

Graph::Graph(): adj_size(1), adj(new Bag<int>*[1]), E(0), V(0) {
  adj[0] = new Bag<int>;
}

Graph::~Graph() {
  /* delete [] adj; */
  for (int i = 0; i < adj_size; i++) {
    delete adj[i];
  }
  delete [] adj;
}

void Graph::addEdge(int j, int k) {

  while (j >= adj_size || k >= adj_size) {
    resize();
  }

  adj[k]->insert(j);
  adj[j]->insert(k);
  E++;
}

int * Graph::adj_array(int j) {
  return adj[j]->array();
}

int Graph::degree(int j) {
  if (j >= adj_size)
    return 0;
  return adj[j]->size();
}

void Graph::resize() {
  Bag<int> ** newadj = new Bag<int>*[adj_size * 2];
  int i = 0;
  for (; i < adj_size; i++) {
    newadj[i] = adj[i];
  }
  adj_size *= 2;
  for (; i < adj_size; i++) {
    newadj[i] = new Bag<int>;
  }
  delete [] adj;
  adj = newadj;
}

void Graph::print() {
  for (int i = 0; i < V; i++) {
    std::cout << i << ": ";
    for (int j = 0; j < adj[i]->size(); j++)
      std::cout << adj_array(i)[j] << "\t";
    std::cout << "\n";
  }
}



/* *************** Cycle ********************* */

Cycle::Cycle(Graph *g): G(g), marked(new bool[G->V])
{
  for (int i = 0; i < G->V; i++)
    marked[i] = false;
}

Cycle::~Cycle() {
  delete [] marked;
}

void Cycle::search(int v, int u)
{
  int w;
  marked[v] = true;
  for (int a = 0; a < G->degree(v); a++) {
    w = G->adj_array(v)[a];
    if (!marked[w])
      search(w, v);
    else if (w != u)
      mhasCycle = true;
  }
}

void Cycle::search(int v, int u, int b)
{
  marked[v] = true;
  int w;
  if (v == b)
    found = true;
  for (int a = 0; a < G->degree(v); a++) {
    w = G->adj_array(v)[a];
    if (!marked[w])
      search(w, v, b);
    else if (w != u)
      mhasCycle = true;
  }
}

bool Cycle::hasCycle()
{
  mhasCycle = false;
  for (int i = 0; i < G->V; i++) {
    marked[i] = false;
  }

  for (int v = 0; v < G->V; v++) {
    if(!marked[v]) {
      search(v, v);
    }
  }
  return mhasCycle;
}

bool Cycle::hasCycle(int a)
{
  mhasCycle = false;
  for (int i = 0; i < G->V; i++) {
    marked[i] = false;
  }
  search(a, a);
  return mhasCycle;
}

bool Cycle::hasCycle(int a, int b)
{
  mhasCycle = false;
  found = false;
  for (int i = 0; i < G->V; i++) {
    marked[i] = false;
  }

  search(a, a, b);
  return found && mhasCycle;
}

/* *************** Components ********************* */

Components::Components(Graph *G): marked(new bool[G->V]),
  id(new int[G->V]),
  count(0),
  sizes(new int[G->V])
{
  for (int i = 0; i < G->V; i++) {
    sizes[i] = 0;
    marked[i] = false;
  }
  for (int s = 0; s < G->V; s++)
    if (!marked[s]) {
      search(G, s);
      count++;
    }
}

Components::~Components() {
  delete [] marked;
  delete [] id;
  delete [] sizes;
}

void Components::search(Graph * G, int v) {
  marked[v] = true;
  id[v] = count;
  sizes[count]++;

  for (int a = 0; a < G->degree(v); a++) {
    int w = G->adj_array(v)[a];
    if (!marked[w])
      search(G, w);
  }
}

bool Components::connected(int v, int w) { return id[v] == id[w]; }
int Components::get_id(int v) { return id[v]; }
int Components::get_count() { return count; }
int Components::component_size(int v) { return sizes[id[v]]; }

double Components::avg_size() {
  int sum = 0;
  for (int i = 0; i < count; i++)
    sum += sizes[i];
  return (double) sum / (double) count;
}

int Components::minsize() {
  int min = numeric_limits<int>::max();
  int s;

  for (int i = 0; i < count; i++) {
    s = sizes[i];
    if (s < min)
      min = s;
  }
  return min;
}

int Components::maxsize_id() {
  int min = 0;
  int s;
  int id = -1;

  for (int i = 0; i < count; i++) {
    s = sizes[i];
    if (s > min) {
      min = s;
      id = i;
    }
  }
  return id;
}

int Components::maxsize() {
  int min = 0;
  int s;

  for (int i = 0; i < count; i++) {
    s = sizes[i];
    if (s > min)
      min = s;
  }
  return min;
}



/* *************** Distance *********************** */

Distance::Distance(Graph * G, int s): marked(new bool[G->V]),
                                      edgeto(new int[G->V]),
                                      s(s)
{
  for (int i = 0; i < G->V; i++) {
    marked[i] = false;
  }
  search(G, s);
}

Distance::~Distance() {
  delete [] marked;
  delete [] edgeto;
}

void Distance::search(Graph * G, int s) {
  queue<int> q;
  int v;

  marked[s] = true;
  q.push(s);
  while(!q.empty()) {
    v = q.front(); q.pop();
    for (int a = 0; a < G->degree(v); a++) {
      int w = G->adj_array(v)[a];
      if (!marked[w]) {
        edgeto[w] = v;
        marked[w] = true;
        q.push(w);
      }
    }
  }
}

bool Distance::haspath(int v) { return marked[v]; }

int Distance::dist(int v) {
  if (!haspath(v)) return -1;
  int count = 0;
  for (int x = v; x != s; x = edgeto[x])
    count++;
  return count;
}
