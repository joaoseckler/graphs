#include "graph.h"
#include <fstream>
#include <string>
#include <locale>
#include <codecvt>
#include <sstream>

using namespace std;

class Grafo {
  private:
    long unsigned int min; // k

  public:
    SGraph<string> G;
    bool parecidas(string s, string t) {

      char cs, ct;
      bool f1 = false, f2 = false; // flags

      if (s.size() == t.size()) {
        for(unsigned int i = 0; i < s.size(); i++) {
          if (t[i] != s[i]) {
            if (!f1) {
              f1 = true;
              cs = s[i];
              ct = t[i];
            }
            else if (!f2) {
              if (cs != t[i] || ct != s[i])
                return false;
              f2 = true;
            }
            else {
              return false;
            }
          }
        }
        return true;
      }

      else if (abs((int)s.size() - (int)t.size()) >= 2)
        return false;

      string menor, maior;
      if (s.size() > t.size()) { menor = t; maior = s; }
      else { menor = s; maior = t; }

      bool removeu = false;
      unsigned int j = 0;
      for (unsigned int i = 0; i < menor.size(); i++) {
        if (maior[j] != menor[i]) {
          if (removeu)
            return false;
          else {
            i--;
            removeu = true;
          }
        }
        j++;
      }
      return true;
    }

    Grafo(int k): min(k) {}

    int insere(string s)
    {
      if (s.length() < min || G.contains(s))
        return -1;
      G.addKey(s);

      int arestas = 0;
      for (int i = 0; i < G.V(); i++) {
        if (G.name(i) != s && parecidas(s, G.name(i))) {
          G.addEdge(s, G.name(i));
          arestas++;
        }
      }

      return arestas;
    }

    int vertices()
    {

      return G.V();
    }

    int arestas()
    {
      return G.E();
    }

    double grau_medio() {
      int sum = 0;

      for (int i = 0; i < G.V(); i++)
        sum += G.get_G()->degree(i);

      return (double) sum / (double) G.V();
    }

    int componentes()
    {
      SComponents<string> C(&G);
      return C.get_count();
    }

    bool conexo()
    {
      SComponents<string> C(&G);
      return C.get_count() == 1;
    }

    int tamComp(string s)
    {
      if (G.contains(s)) {
        SComponents<string> C(&G);
        return C.component_size(s);
      }
      return -1;
        /* Retorna o tamanha da componente conexa onde está a palavra
        ou -1 caso ela não se encontre no grafo */
    }

    double tamComp_media() {
      Components C(G.get_G());
      return C.avg_size();
    }

    int minComp() {
      Components C(G.get_G());
      return C.minsize();
    }

    int maxComp() {
      Components C(G.get_G());
      return C.maxsize();
    }

    string maxComp_exemplos() {
      Components C(G.get_G());
      int id = C.maxsize_id();
      int count = 0;
      string s = "";

      for (int i = 0; i < G.V() && count < 3; i++) {
        if (C.get_id(i) == id) {
          if (count == 0)
            s = G.name(i);
          else
            s += ", " + G.name(i);
          count++;
        }
      }
      return s;

    }

    string maxComp_todas() {
      Components C(G.get_G());
      int id = C.maxsize_id();
      string s = "";

      for (int i = 0; i < G.V(); i++)
        if (C.get_id(i) == id)
            s += "\t" + G.name(i) + "\n";
      return s;
    }

    int dist(string a, string b)
    {
      if (G.contains(a) && G.contains(b)) {
        SDistance<string> D(&G, a);
        return D.dist(b);
        /* Retorna a menor distância entre as palavras a e b ou -1
        caso elas estejam desconexas ou não estejam no grafo */
      }
      return -1;
    }

    double dist_media()
    {
      Distance * D;
      int count = 0, j;
      int d;
      long int sum = 0;

      for (int i = 0; i < G.V() - 1; i++) {
        D = new Distance(G.get_G(), i);
        for (j = i + 1; j < G.V(); j++) {
          if ((d = D->dist(j)) != -1) {
            count++;
            sum += d;
          }
        }
        delete D;
      }

      if (count == 0) return 0;
      return (double) sum/ (double)count;
    }

    bool emCiclo(string a)
    {
      SCycle<string> c(&G);
      bool t = c.hasCycle(a);
      return t;
    }

    bool emCiclo(string a, string b)
    {
      SCycle<string> c(&G);
      return c.hasCycle(a, b);
        /* Retorna verdadeiro casa exista um ciclo que contenha ambas as palavras,
        falso caso contrário */
      return true;
    }

    double densidade() {
      double E = (double) arestas();
      double V = (double) vertices();

      return (2 * E) / (V * (V - 1));
    }
};

bool is_not_word(char c)
{
  /* This supposes there are no utf-8 punctuation marks int the text */
  return(isspace(c)
         || ispunct(c)
         || isblank(c)
         || iscntrl(c));
}


void analisa(string filename, int k) {
  Grafo g(k);

  ifstream file;
  file.open(filename);

  if (!file.is_open()) {
    cout << "Nome de arquivo inválido\n\n";
    file.close();
    exit(EXIT_FAILURE);
  }

  string *s = new string;
  wstring ws;
  char c;
  bool hold = false;

  locale loc("en_US.utf8");
  wstring_convert<codecvt_utf8_utf16<wchar_t>> converter;

  /* Find first alpha char */
  while (file.get(c) && is_not_word(c));
  file.putback(c);

  while(file.get(c)) {
    if (is_not_word(c)) {
      if (!hold) {
        if (!isalpha((*s)[0])) {
          /* Only manipulate as multybyte if first letter of the string
           * is multybyte (this supposes there are no uppercase utf
           * letter in the middle of words) */
          wstring ws = converter.from_bytes(*s);
          ws[0] = tolower(ws[0], loc);
          *s = converter.to_bytes(ws);
        }
        g.insere(*s);
        delete s;
        s = new string();
        hold = true;
      }
    }
    else {
      s->push_back(tolower(c));
      hold = false;
    }
  }

  cout << filename << " a k = " << k << "\n";
  cout << "\tvertices: " << g.vertices() << "\n";
  cout << "\tarestas: " << g.arestas() << "\n";
  cout << "\tcomponentes: " << g.componentes() << "\n";
  cout << "\tconexo: " << g.conexo() << "\n";
  cout << "\ttamanho médio de componente: " << g.tamComp_media() << "\n";
  cout << "\tmenor e maior componentes: " << g.minComp() << ", " << g.maxComp() << "\n";
  cout << "\texemplos da maior componente: " << g.maxComp_exemplos() << "\n";
  cout << "\tdistancia média: " << g.dist_media() << "\n";
  cout << "\tgrau médio: " << g.tamComp_media() << "\n";
  cout << "\tdensidade: " << g.densidade() << "\n";
  /* cout << "\tmaxComp_todas: " << g.maxComp_todas() << "\n"; */
  cout << "\n";

  file.close();
}


int main() {
  string files[5] = {
    "txt/lero.txt",
    "txt/lusiadas.txt",
    "txt/manifest.txt",
    "txt/dict-head.txt",
    "txt/poe.txt",
  };

  for (int j = 2; j < 6; j++)
    for (int i = 0; i < 5; i++)
      analisa(files[i], j);

  return 0;
}

