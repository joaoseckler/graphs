#include <iostream>
#include <algorithm>
using namespace std;

#define DEBUG 1
#define SYMBOL_TABLE


/* ***************************************
   ** Key-value structure ****************
 * ***************************************/


template <typename Key, typename Value>
struct kvpair_s {
  kvpair_s(Key k = nullptr, Value v = nullptr) : key(k), val(v) {}
  ~kvpair_s() {
    delete key;
    delete val;
  }
  Key key;
  Value val;
};

template <typename Key, typename Value>
using kvpair = struct kvpair_s<Key, Value>;

template <typename Key, typename Value>
bool comp_kvpair(kvpair<Key, Value> *p2, kvpair<Key, Value> *p1) {
  return (*(p2->key) < *(p1->key));
}

/* ***************************************
 * ** parent class ***********************
 * ***************************************/
template<typename Key, typename Value>
class SymbolTable
{
  public:
    virtual ~SymbolTable() {};
    virtual void insere(Key, Value) = 0;
    virtual Value devolve(Key) = 0;
    virtual void remove(Key) = 0;
    virtual int rank(Key) = 0;
    virtual Key seleciona(int) = 0;
#if DEBUG == 1
    virtual void print() = 0;
#endif
};


