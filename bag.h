#ifndef BAG_H
#define BAG_H

template <class Item>
class Bag
{
  private:
    Item * v;
    int n;
    int v_size;
    void resize(int size);

  public:
    Bag();
    ~Bag();
    void insert(Item item);
    bool empty();
    int nElements();
    Item at(int i);
    Item * array();
    int size();
};

template <class Item>
Bag<Item>::Bag(): v(new Item[1]), n(0), v_size(1) {}

template <class Item>
Bag<Item>::~Bag()
{
  if (v != nullptr)
    delete [] v;
}

template <class Item>
void Bag<Item>::insert(Item item)
{
  if (n == v_size)
    resize(2*v_size); // Agora acreditamos que tem memória no computador
  v[n] = item;
  n++;
}

template <class Item>
bool Bag<Item>::empty()
{
  return (n == 0);
}

template <class Item>
int Bag<Item>::nElements()
{
  return(n);
}

template <class Item>
Item Bag<Item>::at(int i)
{
  if (i >= 0 && i < n)
    return(v[i]);
  /* Vai dar warning porque não cobre todos os casos, mas pro nosso caso
   * tá bom */
}

template <class Item>
void Bag<Item>::resize(int t)
{
  Item * novoV = new Item[t];
  for (int i = 0; i < n; i++)
    novoV[i] = v[i];
  delete [] v;
  v = novoV;
  v_size = t;
}

template <class Item>
Item * Bag<Item>::array()
{
  return v;
}

template <class Item>
int Bag<Item>::size()
{
  return n;
}

#endif // BAG_H
