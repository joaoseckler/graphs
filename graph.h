#include <iostream>
#include <algorithm>
#include <stack>
#include <queue>
#include <limits>
#include "bag.h"
#include "rn.h"
using namespace std;


class Graph
{
  private:
    int adj_size;
    Bag<int> ** adj; // Lista de adjacências
    void resize();
  public:
    Graph();
    ~Graph();
    int E;
    int V;
    void addEdge(int j, int k);
    int * adj_array(int j);
    int degree(int j);
    void print();
};

template <class Key>
class SGraph
{
  private:
    RN<Key*, int*> st;
    Graph G;
    int curr_i;
    Key ** keys;
    int keys_size;
    void resize_keys();

  public:
    SGraph();
    ~SGraph();
    bool contains(Key s);
    int index(Key s);
    Key name(int k);
    void addKey(Key k);
    void addEdge(Key k, Key l);
    int V();
    int E();
    Graph * get_G();
    void print();
    Key ** get_keys();
};


template <class Key>
SGraph<Key>::SGraph(): curr_i(0), keys(new Key*[1]), keys_size(1) {}

template <class Key>
SGraph<Key>::~SGraph() {
  for (int i = 0; i < keys_size; i++)
    keys[i] = nullptr;
  delete [] keys;
}

template <class Key>
bool SGraph<Key>::contains(Key s)
{
  return bool(st.devolve(&s));
}

template <class Key>
void SGraph<Key>::addKey(Key s)
{
  if (curr_i >= keys_size)
    resize_keys();

  string * n = new string(s);

  keys[curr_i] = n;
  st.insere(n, new int(curr_i));
  curr_i++;
  G.V++;
}

template <class Key>
void SGraph<Key>::addEdge(Key s, Key l)
{
  int *i = st.devolve(&s), *j = st.devolve(&l);

  if (i != nullptr && j != nullptr) {

    G.addEdge(*i, *j);
  }
}

template <class Key>
int SGraph<Key>::index(Key s)
{
  int *i = st.devolve(&s);
  if (i != nullptr)
    return *i;
  return -1;
}

template <class Key>
Key SGraph<Key>::name(int k)
{
  if (k < G.V)
    return * keys[k];
  return nullptr;
}

template <class Key>
void SGraph<Key>::resize_keys()
{
  Key ** newk = new Key*[keys_size * 2];
  for (int i = 0; i < keys_size; i++) {
    newk[i] = keys[i];
    keys[i] = nullptr;
  }
  delete [] keys;
  keys = newk;
  keys_size *= 2;
}

template <class Key>
int SGraph<Key>::V() { return G.V; }

template <class Key>
int SGraph<Key>::E() {return G.E;}

template <class Key>
Graph * SGraph<Key>::get_G() {return &G;}

template <class Key>
void SGraph<Key>::print() {
  for (int i = 0; i < G.V; i++) {
    std::cout << name(i) << ":\t\t";
    for (int j = 0; j < G.degree(i); j++)
      std::cout << name(G.adj_array(i)[j]) << "\t";
    std::cout << "\n";
  }

}

template <class Key>
Key ** SGraph<Key>::get_keys() {
  return keys;
}

class Cycle
{
  private:
    Graph * G;
    bool * marked;
    bool mhasCycle;
    bool found;
    void search(int v, int u);
    void search(int v, int u, int b);
  public:
    Cycle(Graph * G);
    ~Cycle();
    bool hasCycle();
    bool hasCycle(int a);
    bool hasCycle(int a, int b);
};

template <class Key>
class SCycle
{
  private:
    SGraph<Key> * G;
    Cycle C;
  public:
    SCycle(SGraph<Key> * SG);
    ~SCycle() {};
    bool hasCycle();
    bool hasCycle(Key a);
    bool hasCycle(Key a, Key b);
};

template <class Key>
SCycle<Key>::SCycle(SGraph<Key> * g): G(g), C(Cycle(g->get_G())) {}

template <class Key>
bool SCycle<Key>::hasCycle()
{
  return C.hasCycle();
}

template <class Key>
bool SCycle<Key>::hasCycle(Key a)
{
  return C.hasCycle(G->index(a));
}

template <class Key>
bool SCycle<Key>::hasCycle(Key a, Key b)
{
  return C.hasCycle(G->index(a), G->index(b));
}

/********* Components *************/

class Components
{
  private:
    bool * marked;
    int * id;
    int count;
    void search(Graph * G, int v);
    int * sizes;
  public:
    Components(Graph * G);
    ~Components();
    bool connected(int v, int w);
    int get_id(int v);
    int get_count();
    int component_size(int v); // Size of component by component id
    double avg_size();
    int minsize();
    int maxsize();
    int maxsize_id();
};

template <class Key>
class SComponents
{
  private:
    SGraph<Key> *G;
    Components C;
  public:
    SComponents(SGraph<Key> * G);
    ~SComponents() {};
    bool connected(Key v, Key w);
    int get_id(Key v);
    int get_count();
    int component_size(Key s); // Size of component by Key
};

template <class Key>
SComponents<Key>::SComponents(SGraph<Key> * g): G(g), C(Components(g->get_G())) {}

template <class Key>
bool SComponents<Key>::connected(Key v, Key w)
{
  return C.connected(G->index(v), G->index(w));
}

template <class Key>
int SComponents<Key>::get_id(Key v)
{
  return C.get_id(G->index(v));
}

template <class Key>
int SComponents<Key>::get_count()
{
  return C.get_count();
}

template <class Key>
int SComponents<Key>::component_size(Key v)
{
  return C.component_size(G->index(v));
}


/********* Distance ***************/

class Distance
{
  private:
    bool * marked;
    int * edgeto;
    int s;
    void search(Graph * G, int v); // Breadth-first
  public:
    Distance(Graph * G, int);
    ~Distance();
    bool haspath(int v);
    int dist(int v);
};

template <class Key>
class SDistance
{
  private:
    SGraph<Key> * G;
    Distance D;
  public:
    SDistance(SGraph<Key> * G, Key v);
    bool haspath(Key v);
    int dist(Key v);
};

template <class Key>
SDistance<Key>::SDistance(SGraph<Key> * g, Key v):
  G(g),
  D(Distance(g->get_G(), g->index(v))) {}

template <class Key>
bool SDistance<Key>::haspath(Key v) { return D.haspath(G->index(v)); }

template <class Key>
int SDistance<Key>::dist(Key v) { return D.dist(G->index(v)); }
