#include "st.h"

#ifndef SYMBOL_TABLE
#error "Trying to include rn.h withou including st.h"
#endif

/* ***************************************
 * ** Red-Black Tree *********************
 * ***************************************/

template <typename Key, typename Value>
struct rnnode_s {
  rnnode_s(Key key = nullptr, Value val = nullptr, bool r = true) {
    info.key = key;
    info.val = val;
    left = nullptr;
    right = nullptr;
    father = nullptr;
    red = r;
  }

  kvpair<Key, Value> info;
  rnnode_s * left;
  rnnode_s * right;
  rnnode_s * father;
  bool red;
};

template <typename Key, typename Value>
using rnnode = struct rnnode_s<Key, Value>;


/* Key and Value types should be pointers */
template <typename Key, typename Value>
class RN: public SymbolTable<Key, Value>
{
  private:
    rnnode<Key, Value> *nil;
    rnnode<Key, Value> *root;

    void leftrotate(rnnode<Key, Value> *);
    void rightrotate(rnnode<Key, Value> *);
    void fixinsere(rnnode<Key, Value> *);
    void fixremove(rnnode<Key, Value> *);
    void transplant(rnnode<Key, Value> *, rnnode<Key, Value> *);

    void delnodes(rnnode<Key, Value> *p);
    int countnodes(rnnode<Key, Value>*);
    void rprint(rnnode<Key, Value>*, int);
  public:
    RN();
    ~RN();
    void insere(Key, Value);
    Value devolve(Key);
    void remove(Key);
    int rank(Key);
    Key seleciona(int);
#if DEBUG == 1
    void print();
#endif
};

template <typename Key, typename Value>
RN<Key, Value>::RN()
{
  nil = new rnnode<Key, Value>(nullptr, nullptr, false);
  nil->right = nil->left = nil->father = nil;
  root = nil;
}

template <typename Key, typename Value>
RN<Key, Value>::~RN()
{
  delnodes(root);
  delete nil;
}

template <typename Key, typename Value>
void RN<Key, Value>::delnodes(rnnode<Key, Value> *p)
{
  if (p != nil) {
    delnodes(p->left);
    delnodes(p->right);
    delete p;
  }
}

template <typename Key, typename Value>
void RN<Key, Value>::leftrotate(rnnode<Key, Value> *p)
{
  rnnode<Key, Value> *q;

  q = p->right;
  p->right = q->left;
  if (p->right != nil)
    p->right->father = p;
  q->left = p;
  q->father = p->father;
  if (p->father == nil)
    root = q;
  else if (p == p->father->left)
    p->father->left = q;
  else
    p->father->right = q;
  p->father = q;
}

template <typename Key, typename Value>
void RN<Key, Value>::rightrotate(rnnode<Key, Value> *p)
{
  rnnode<Key, Value> *q;

  q = p->left;
  p->left = q->right;
  if (p->left != nil)
    p->left->father = p;
  q->right = p;
  q->father = p->father;
  if (p->father == nil)
    root = q;
  else if (p == p->father->left)
    p->father->left = q;
  else
    p->father->right = q;
  p->father = q;
}

template <typename Key, typename Value>
void RN<Key, Value>::fixinsere(rnnode<Key, Value> *p)
{
  rnnode<Key, Value> *q = root;

  while (p->father->red) {
    if (p->father == p->father->father->left) {
      q = p->father->father->right; /* q is p's uncle */

      if (q->red) {
        p->father->red = false;
        q->red = false;
        p->father->father->red = true;
        p = p->father->father;
      }
      else {
        if (p == p->father->right) {
          p = p->father;
          leftrotate(p);
        }
        p->father->red = false;
        p->father->father->red = true;
        rightrotate(p->father->father);
      }
    }

    else { /*(p->father == p->father->father->right)*/
      q = p->father->father->left; /* q is p's uncle */

      if (q->red) {
        p->father->red = false;
        q->red = false;
        p->father->father->red = true;
        p = p->father->father;
      }
      else {
        if (p == p->father->left) {
          p = p->father;
          rightrotate(p);
        }
        p->father->red = false;
        p->father->father->red = true;
        leftrotate(p->father->father);
      }

    }
  }
  root->red = false;
}

template <typename Key, typename Value>
void RN<Key, Value>::insere(Key key, Value val)
{
  rnnode<Key, Value> *p = root, *q = nil;

  while (p != nil) {
    q = p;
    if (*key < *(p->info.key)) {
      p = p->left;
    }

    else if (*key > *(p->info.key)) {
      p = p->right;
    }

    else {
      delete p->info.val;
      p->info.val = val;
      delete key;
      return;
    }
  }

  p = new rnnode<Key, Value>(key, val, true);

  p->father = q;
  if (q == nil)
    root = p;
  else if (*(p->info.key) < *(q->info.key))
    q->left = p;
  else
    q->right = p;
  p->left = nil;
  p->right = nil;

  fixinsere(p);

}

template <typename Key, typename Value>
Value RN<Key, Value>::devolve(Key key)
{

  rnnode<Key, Value> *p = root;
  while (p != nil) {
    if (*key < *(p->info.key))
      p = p->left;
    else if (*key > *(p->info.key))
      p = p->right;
    else
      return (p->info.val);

  }
  return nullptr;
}

template <typename Key, typename Value>
void RN<Key, Value>::transplant(rnnode<Key, Value> *p,
                                rnnode<Key, Value> *q)
{
  if (p->father == nil)
    root = q;
  else if (p->father->left == p)
    p->father->left = q;
  else
    p->father->right = q;

  q->father = p->father;
}

template <typename Key, typename Value>
void RN<Key, Value>::fixremove(rnnode<Key, Value> *p)
{
  rnnode<Key, Value> *q;

  while (p != root && !(p->red)) {
    if (p == p->father->left) {
      q = p->father->right;
      if (q->red) {
        q->red = false;
        p->father->red = true;
        leftrotate(p->father);
        q = p->father->right;
      }
      if (!(q->left->red) && !(q->right->red)) {
        q->red = true;
        p = p->father;
      }
      else {
        if (!(q->right->red)) {
          q->left->red = false;
          q->red = true;
          rightrotate(q);
          q = p->father->right;
        }
        q->red = p->father->red;
        p->father->red = false;
        q->right->red = false;
        leftrotate(p->father);
        p = root;
      }
    }

    else { /* p == p->father->right */
      q = p->father->left;
      if (q->red) {
        q->red = false;
        p->father->red = true;
        rightrotate(p->father);
        q = p->father->left;
      }
      if (!q->right->red && !q->left->red) {
        q->red = true;
        p = p->father;
      }
      else {
        if (!q->left->red) {
          q->right->red = false;
          q->red = true;
          leftrotate(q);
          q = p->father->left;
        }
        q->red = p->father->red;
        p->father->red = false;
        q->left->red = false;
        rightrotate(p->father);
        p = root;
      }
    }
  }
  p->red = false;
}

template <typename Key, typename Value>
void RN<Key, Value>::remove(Key key)
{

  rnnode<Key, Value> *p = root;

  while (p != nil) {
    if (*key < *(p->info.key)) {
      p = p->left;
    }

    else if (*key > *(p->info.key)) {
      p = p->right;
    }

    else { /* Delete p */
      rnnode<Key, Value> *t = p, *q;
      bool original_color = t->red;

      if (p->left == nil) {
        q = p->right;
        transplant(p, q);
      }
      else if (p->right == nil) {
        q = p->left;
        transplant(p, q);
      }
      else {
        t = p->right;
        while (t->left != nil)
          t = t->left;
        /* t is minimum of p->right */

        original_color = t->red;
        q = t->right;
        if (t->father == p)
          q->father = t;
        else {
          transplant(t, t->right);
          t->right = p->right;
          t->right->father = t;
        }
        transplant(p, t);
        t->left = p->left;
        t->left->father = t;
        t->red = p->red;
        if (!original_color)
          fixremove(q);
      }

      delete p;
      return;
    }
  }
}

template <typename Key, typename Value>
int RN<Key, Value>::rank(Key key)
{
  rnnode<Key, Value> *p = root;
  int r = 0;

  while (p != nil) {
    if (*key < *(p->info.key)) {
      p = p->left;
    }
    else if (*key > *(p->info.key)) {
      r = r + 1 + countnodes(p->left);
      p = p->right;
    }
    else
      return r + countnodes(p->left);
  }
  return r;
}

template <typename Key, typename Value>
Key RN<Key, Value>::seleciona(int k)
{
  rnnode<Key, Value> *p = root;
  int r = countnodes(p->left);

  while (p != nil) {
    if (r > k) {
      p = p->left;
      if (p)
        r = r - 1 - countnodes(p->right);
    }
    else if (r < k) {
      p = p->right;
      if (p)
        r = r + 1 + countnodes(p->left);
    }
    else
      return p->info.key;
  }
  return nullptr;
}

template <typename Key, typename Value>
int RN<Key, Value>::countnodes(rnnode<Key, Value> *p)
{
  if (p != nil)
    return 1 + countnodes(p->left) + countnodes(p->right);
  else
    return 0;
}


#if DEBUG == 1
template <typename Key, typename Value>
void RN<Key, Value>::print()
{
  rprint(root, 0);
}

template <typename Key, typename Value>
void RN<Key, Value>::rprint(rnnode<Key, Value> *p, int space)
{
  if (p == nil)
    return;
  rprint(p->right, space + 1);

  for (int i = 0; i < space; i++)
    cout << "   ";
  cout << *(p->info.val) << ": " << *(p->info.key);
  if (p->red)
    cout << "(r)";
  else
    cout << "(n)";
  cout << "\n";

  rprint(p->left, space + 1);
}
#endif
